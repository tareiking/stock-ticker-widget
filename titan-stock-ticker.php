<?php
/**
 * Titan Stock Ticker Widget 
 *
 * @package   Titan Stock Ticker
 * @author    Tarei King
 * @license   GPL-2.0+
 * @link      http://sennza.com.au
 * @copyright 2014 Your Name or Company Name
 *
 * @wordpress-plugin
 * Plugin Name:       Titan Stock Ticker Widget
 * Plugin URI:        http://sennza.com.au
 * Description:       Creates a shortcode widget to print out a specified Stock Ticker from Yahoo with a 20 minute delay.
 * Version:           1.0.0
 * Author:            Tarei King, Bronson Quick, Lachlan Macpherson, Ryan Mccue
 * Author URI:        http://sennza.com.au
 * Text Domain:       titan-stock-ticker
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-titan-stock-ticker.php' );

/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
register_activation_hook( __FILE__, array( 'Titan_Stock_Ticker', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Titan_Stock_Ticker', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'Titan_Stock_Ticker', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * @TODO:
 *
 * - replace `class-plugin-name-admin.php` with the name of the plugin's admin file
 * - replace Plugin_Name_Admin with the name of the class defined in
 *   `class-plugin-name-admin.php`
 *
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/titan-stock-ticker-admin.php' );
	add_action( 'plugins_loaded', array( 'Titan_Stock_Ticker_Admin', 'get_instance' ) );

}
